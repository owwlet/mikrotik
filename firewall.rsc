# jan/05/2018 06:06:54 by RouterOS 6.39.3
# software id = J6IC-PWLI
#
/ip firewall address-list
add address=0.0.0.0/8 comment="BOGON adresses" list=BOGON
add address=10.0.0.0/8 list=BOGON
add address=100.64.0.0/10 list=BOGON
add address=127.0.0.0/8 list=BOGON
add address=169.254.0.0/16 list=BOGON
add address=172.16.0.0/12 list=BOGON
add address=192.0.0.0/24 list=BOGON
add address=192.0.2.0/24 list=BOGON
add address=192.168.0.0/16 list=BOGON
add address=198.18.0.0/15 list=BOGON
add address=198.51.100.0/24 list=BOGON
add address=203.0.113.0/24 list=BOGON
add address=224.0.0.0/4 list=BOGON
add address=240.0.0.0/4 list=BOGON
/ip firewall filter
add action=drop chain=input comment="bogon drop" in-interface=ether1 \
    src-address-list=BOGON
add action=accept chain=forward connection-nat-state=dstnat
add action=accept chain=input protocol=icmp
add action=accept chain=input connection-state=established in-interface=\
    ether1
add action=accept chain=input connection-state=related in-interface=ether1
add action=drop chain=input in-interface=ether1
add action=jump chain=forward in-interface=ether1 jump-target=customer
add action=accept chain=customer connection-state=established
add action=accept chain=customer connection-state=related
add action=drop chain=customer
add action=accept chain=forward connection-state=established,related
add action=accept chain=forward connection-state=established,related
add action=drop chain=forward dst-address=192.168.1.0/24 src-address=\
    10.11.12.0/24
add action=drop chain=forward dst-address=10.11.12.0/24 src-address=\
    192.168.1.0/24
/ip firewall nat
add action=masquerade chain=srcnat out-interface=ether1
add action=netmap chain=dstnat comment="NAS webdav" dst-port=5006 \
    in-interface=ether1 protocol=tcp src-port=5006 to-addresses=192.168.1.75 \
    to-ports=5006
add action=netmap chain=dstnat dst-port=5006 in-interface=ether1 protocol=udp \
    src-port=5006 to-addresses=192.168.1.75 to-ports=5006
add action=netmap chain=dstnat comment="webserver 1C" dst-port=5438 \
    in-interface=ether1 protocol=tcp src-port=5438 to-addresses=192.168.1.73 \
    to-ports=5438
add action=netmap chain=dstnat dst-port=5438 in-interface=ether1 protocol=udp \
    src-port=5438 to-addresses=192.168.1.73 to-ports=5438
add action=netmap chain=dstnat comment="Synology Notes" dst-port=5001 \
    in-interface=ether1 protocol=tcp src-port=5001 to-addresses=192.168.1.75 \
    to-ports=5001
/ip firewall service-port
set sip disabled=yes sip-timeout=5m
